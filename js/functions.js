$(function() {
    
    // Tabien alustus
    $("#tabs").tabs();
    
    // Tabien fade-efekti klikatessa
    $("#tabs").tabs({
        show: {effect: "fade", duration: 300}, 
        hide: {effect: "fade", duration: 300}
    });
    
    // Fokuksen asettaminen etaisyys-kenttään alussa
    $(document).ready(function() {
        $("#etaisyys").focus();
    });
    
    // Laske-painiketta painamalla kutsutaan laskeArvo-funktiota
    $("#laske").click(function() {
        laskeArvo();
    });
    
    // Tyhjennä-painiketta painamalla resetoidaan kentät ja kuva ja asetetaan
    // fokus kuten edellä
    $("#tyhjenna").click(function() {
        $("input[type=number]").val("");
        $("#successMessage").html("");
        $("#errorMessage").html("");
        $("#image").html("<img src='images/laskin.png'>");
        $("#etaisyys").focus();
    });
    
    // Klikkaamalla kenttiä resetoidaan viestikentät ja kuva
    $("input[type=number]").focus(function() {
        $("#successMessage").html("");
        $("#errorMessage").html("");
        $("#image").html("<img src='images/laskin.png'>");
    });
    
    // Funktio, jossa tarkastetaan syötteet ja lasketaan puuttuva arvo annettujen
    // syötteiden perusteella
    function laskeArvo() {
        
        // Syötteiden lukeminen muuttujiin
        var etaisyys = $("#etaisyys").val();
        var vauhti = $("#vauhti").val();
        var tunnit = $("#tunnit").val();
        var minuutit = $("#minuutit").val();
        var sekunnit = $("#sekunnit").val();
        
        // Muuttuja, jossa säilytetään syötteiden määrä
        var syotteet = 0;
        
        // Näissä ehtolauseissa lasketaan annettujen syötteiden määrä
        if (etaisyys !== "") {
            syotteet++;
        }
        
        if (vauhti !== "") {
            syotteet++;
        }
        
        if (tunnit !== "" && minuutit !== "") {
            syotteet++;
        }
        
        // Syötteiden määrän perusteella annetaan sopiva viesti
        if (syotteet === 0) {
            $("#errorMessage").html("Et antanut mitään syötteitä. Anna kaksi syötettä.");
            return false;
        }
        
        if (syotteet === 1) {
            $("#errorMessage").html("Annoit vain yhden syötteen. Lisää vielä toinen.");
            return false;
        }
        
        if (syotteet === 3) {
            $("#errorMessage").html("Annoit liikaa syötteitä. Poista yksi.");
            return false;
        }
        
        // Syötteiden tarkistus
        if (etaisyys < 0 || vauhti < 0 || tunnit < 0) {
            $("#errorMessage").html("Et voi antaa negatiivisia arvoja.");
            return false;
        }
        
        if (minuutit < 0 || minuutit > 59) {
            $("#errorMessage").html("Minuuttien on oltava väliltä 0 - 59.");
            return false;
        }
        
        if (sekunnit < 0 || sekunnit > 59) {
            $("#errorMessage").html("Sekuntien on oltava väliltä 0 - 59.");
            return false;
        }
        
        // Varsinainen laskeminen voidaan aloittaa, kun syötteitä on oikea määrä
        // ja niissä ei ole virheitä.
        if (syotteet === 2) {
            
            // Asetetaan sekunnit nollaan, jos niitä ei ole syötetty
            if (sekunnit === "") {
                sekunnit = 0;
            }
            
            // Aika-muuttujassa tunnit, minuutit ja sekunnit muutetaan tunneiksi.
            // Tämä aiheutti päänvaivaa. "+" muuttujien edessä varmistaa, että
            // lasketaan yhteen lukuja, ilman tätä laskettaisiin yhteen stringejä,
            // joka taas aiheutti laskuvirheitä.
            var aika = +tunnit + +minuutit/60 + +sekunnit/3600;
            
            // Puuttuvan kentän perusteella päätellään, mitä halutaan laskea,
            // syötetään tekstikenttään haluttu tulos ja vaihdetaan kuva.
            if (etaisyys === "") {
                etaisyys = (vauhti * aika).toFixed(2);
                $("#successMessage").html("Etäisyys, jonka saavutat on <b>" 
                        + etaisyys + " km.</b>");
                $("#image").html("<img src='images/etaisyys.png'>");
            }
            
            if (vauhti === "") {
                vauhti = (etaisyys / aika).toFixed(2);
                $("#successMessage").html("Keskivauhtisi pitää olla <b>" 
                        + vauhti + " km/h.</b>");
                $("#image").html("<img src='images/vauhti.png'>");
            }
            
            if (tunnit === "" || minuutit === "") {
                aika = (etaisyys / vauhti);
                
                // Aika-muuttujan tuntien muuttaminen takaisin tunneiksi,
                // minuuteiksi ja sekunneiksi. Tämäkin oli hankalahkoa.
                sekunnit = aika * 3600;
                tunnit = Math.floor(sekunnit / 3600);
                sekunnit = sekunnit % 3600;
                minuutit = Math.floor(sekunnit / 60);
                sekunnit = (sekunnit % 60).toFixed(0);
                $("#successMessage").html("Tarvitset aikaa <b>" + tunnit + 
                        " tuntia, " + minuutit + " minuuttia ja " + sekunnit + 
                        " sekuntia.</b>");
                $("#image").html("<img src='images/aika.png'>");
            }    
        }
    }
});
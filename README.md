# Distance-Speed-Time calculator #

http://pitkamatkaeipelota.fi/dstcalculator/

### FI ###

JavaScriptin jQuery- ja jQuery UI -kirjastoilla toteutettu laskuri, joka laskee joko etäisyyden, vauhdin tai ajan riippuen siitä mitkä kaksi muuta arvoa siihen syöttää.

Toteutettu harjoitustyönä käyttöliittymäohjelmoinnin kurssilla.

### EN ###

This calculator, written in JavaScript with jQuery and jQuery UI libraries, calculates the third variable from distance, speed and time when given the other two.

Done as a school project in an UI programming course.

The interface is in Finnish.